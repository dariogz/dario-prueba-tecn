<?php
namespace Poker;

class Hand
{
    public static function isStraight(array $cards): bool
    {
        // 1. Lenght validation
        $totalCards = count($cards);
        if ($totalCards < 5 OR $totalCards > 8)
            return false;

        // 2. Look for an Ace
        $validCards = $cards;
        if (in_array(14, $cards))
            $validCards[]= 1;

        // 3. Sorting and Looping through cards
        sort($validCards);
        $lastCard = 0;
        $consecutive = 0;
        for ($i = 0; $i < count($validCards); $i++) {
            $card = (int) $validCards[$i];
            // Card is repeated
            if ($card == $lastCard)
                continue;
            // Card is in consecutive order
            else if ($card == $lastCard + 1) {
                $lastCard++;
                $consecutive++;
            }
            // Card is not in consecutive order
            else {
                $consecutive = 1;
                $lastCard = $card;
            }

            // Is Straight
            if ($consecutive == 5) {
                return true;
            }
        }

        // Not is Straight
        return false;
    }
}