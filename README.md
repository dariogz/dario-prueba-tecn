# Prueba Técnica

Repositorio con el caso de prueba de la funcion PHP y la base de datos para el testeo de las queries.

### Requerimientos

PHP >= 7.1
Composer

### Instalacion y correr test con PHPUnit

```
composer install
composer dump-autoload
./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests/PokerTest
```